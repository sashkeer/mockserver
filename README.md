### Mock server

Установи зависимости:
```bash
npm i
```

Запусти сервер:
```bash
npm start
```

По умолчанию порт 8080

POST Запросы на:
- http://localhost:8080/start
- http://localhost:8080/end
- http://localhost:8080/preparation
- http://localhost:8080/ready
